# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2022-05-31 09:31+0200\n"
"PO-Revision-Date: 2012-07-30 06:34-0000\n"
"Last-Translator: lucas alkaid <alkaid@ime.usp.br>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Audio\"]]\n"
msgid "[[!meta title=\"Sound and video\"]]\n"
msgstr "[[!meta title=\"Audio\"]]\n"

#. type: Plain text
msgid "Tails includes several sound and video applications:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[*Audacity*](https://www.audacityteam.org/), a multi-track audio editor."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    See the [official documentation](http://manual.audacityteam.org/).\n"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[*Brasero*](https://wiki.gnome.org/Apps/Brasero), an application to burn, "
"copy, and erase CDs and DVDs."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    See the [official documentation](https://help.gnome.org/users/brasero/stable/).\n"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[*Sound Juicer*](https://wiki.gnome.org/Apps/SoundJuicer), a CD ripper "
"application."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[*Sound Recorder*](https://wiki.gnome.org/Apps/SoundRecorder), a sound "
"recorder."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"    Sound clips recorded using *Sound Recorder* are saved to the *Recordings*\n"
"    folder.\n"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[*Videos*](https://wiki.gnome.org/Apps/Videos), an audio and video player."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"These applications can be started from the\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Sound & Video</span></span> menu.\n"
msgstr ""

#. type: Plain text
msgid ""
"You can also install using the [[Additional Software|doc/first_steps/"
"additional_software]] feature:"
msgstr ""

#. type: Bullet: '- '
msgid ""
"**[*VLC*](https://www.videolan.org/vlc/)**, a multimedia player with "
"advanced features."
msgstr ""

#. type: Plain text
msgid "- **[*Pitivi*](https://www.pitivi.org/)**, a video editor."
msgstr ""

#. type: Bullet: '- '
msgid ""
"**[*Cheese*](https://wiki.gnome.org/Apps/Cheese)**, an application to take "
"pictures and videos from your webcam."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents/persistence\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents/metadata.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#, fuzzy
#~| msgid ""
#~| "[Audacity](http://audacity.sourceforge.net) is a multi-track audio "
#~| "editor for Linux/Unix, MacOS and Windows. It is designed for easy "
#~| "recording, playing and editing of digital audio."
#~ msgid ""
#~ "**<span class=\"application\">[Audacity](https://www.audacityteam.org/)</"
#~ "span>** is a multi-track audio editor designed for recording, playing and "
#~ "editing of digital audio.  See the [official documentation](http://manual."
#~ "audacityteam.org/)."
#~ msgstr ""
#~ "[Audacity](http://audacity.sourceforge.net) é um editor de áudio "
#~ "multipista para Linux/Unix, MacOS e Windows. Ele é designado para fácil "
#~ "gravação, reprodução e edição de áudio digital."

#, fuzzy
#~| msgid ""
#~| "[Audacity](http://audacity.sourceforge.net) is a multi-track audio "
#~| "editor for Linux/Unix, MacOS and Windows. It is designed for easy "
#~| "recording, playing and editing of digital audio."
#~ msgid ""
#~ "**<span class=\"application\">[Brasero](https://wiki.gnome.org/Apps/"
#~ "Brasero)</span>** is an application to burn, copy, and erase CD and DVD "
#~ "media: audio, video, or data.  See the [official documentation](https://"
#~ "help.gnome.org/users/brasero/stable/)."
#~ msgstr ""
#~ "[Audacity](http://audacity.sourceforge.net) é um editor de áudio "
#~ "multipista para Linux/Unix, MacOS e Windows. Ele é designado para fácil "
#~ "gravação, reprodução e edição de áudio digital."

#, fuzzy
#~| msgid ""
#~| "[Audacity](http://audacity.sourceforge.net) is a multi-track audio "
#~| "editor for Linux/Unix, MacOS and Windows. It is designed for easy "
#~| "recording, playing and editing of digital audio."
#~ msgid ""
#~ "**<span class=\"application\">[Pitivi](http://pitivi.org/)</span>** is a "
#~ "video editor.  See the [official documentation](http://pitivi.org/"
#~ "manual/)."
#~ msgstr ""
#~ "[Audacity](http://audacity.sourceforge.net) é um editor de áudio "
#~ "multipista para Linux/Unix, MacOS e Windows. Ele é designado para fácil "
#~ "gravação, reprodução e edição de áudio digital."
